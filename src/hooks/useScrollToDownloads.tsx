const useScrollToDownloads = () => {
  const handleDownloadClick = (): void => {
    window.scrollTo({
      top: 1600,
      left: 0,
      behavior: "smooth",
    });
  };

  return { handleDownloadClick };
};

export default useScrollToDownloads;
