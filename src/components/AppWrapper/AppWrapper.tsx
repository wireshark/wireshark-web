import { FunctionComponent } from "preact";
import { useEffect, useState } from "preact/hooks";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import { THEME_OPTIONS } from "../../utils/types";

const AppWrapper: FunctionComponent = ({ children }) => {
  const [isLightMode, setIsLightMode] = useState(navigator.userAgent.includes("Chrome") ? (window.matchMedia && window.matchMedia('(prefers-color-scheme: light)').matches) && localStorage.getItem("theme") === "theme-light" : (window.matchMedia && window.matchMedia('(prefers-color-scheme: light)').matches) || localStorage.getItem("theme") === "theme-light");

  useEffect(() => {
    if (isLightMode) {
      localStorage.setItem("theme", THEME_OPTIONS.LIGHT);
      document.documentElement.className = THEME_OPTIONS.LIGHT
    } else {
      localStorage.setItem("theme", THEME_OPTIONS.DARK)
      document.documentElement.className = THEME_OPTIONS.DARK
    }
  }, [isLightMode])

  return (
    <>
      <Header isLightMode={isLightMode} setIsLightMode={setIsLightMode} />
      <div style={{ marginTop: "130px" }}>{children}</div>
      <Footer isLightMode={isLightMode} />
    </>
  );
};

export default AppWrapper;
