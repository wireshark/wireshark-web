import { FunctionComponent } from "preact";
import { useEffect, useState, useReducer } from "preact/hooks";

const StringCaptureFilter: FunctionComponent = () => {
    const [searchOff, setSearchOff] = useState("");
    const [searchStr, setSearchStr] = useState("");
    const [filterStr, setFilterStr] = useState("");
    const [, forceUpdate] = useReducer(x => x + 1, 0);

    function utf8Convert(string) {
        var utf8bytes = [];

        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);

            if (c < 128) {
                utf8bytes.push(c);
            } else if (c > 127 && c < 2048) {
                utf8bytes.push((c >> 6) | 192);
                utf8bytes.push((c & 63) | 128);
            } else if (c > 2047 && c < 65536) {
                utf8bytes.push((c >> 12) | 224);
                utf8bytes.push(((c >> 6) & 63) | 128);
                utf8bytes.push((c & 63) | 128);
            }
            // else skip this character (for now).
        }
        return utf8bytes;
    }

    function stringToBpf() {
        let offset = searchOff
        const bytes = utf8Convert(searchStr);
        let snippet = "";
        let slicelen;
        let hexbytes;
        let conj = "";
        let offstr;

        setFilterStr(snippet);

        while (bytes.length > 0) {
            if (bytes.length >= 4) {
                slicelen = 4;
            } else if (bytes.length >= 2) {
                slicelen = 2;
            } else {
                slicelen = 1;
            }

            let slicebytes = bytes.slice(0, slicelen);
            bytes.splice(0, slicelen);

            hexbytes = "";
            for (var i = 0; i < slicebytes.length; i++) {
                hexbytes += slicebytes[i].toString(16);
            }

            if (offset) {
                offstr = " + " + offset;
            } else {
                offstr = "";
            }

            snippet +=
                conj +
                "tcp[((tcp[12:1] & 0xf0) >> 2)" +
                offstr +
                ":" +
                slicelen +
                "] = 0x" +
                hexbytes;

            conj = " && ";
            offset += slicelen;
        }

        if (snippet.length < 1) {
            snippet =
                'enter a string to match';
        }

        setFilterStr(snippet);
    }

    useEffect(() => {
        stringToBpf();
    }, [searchOff, searchStr, filterStr])

    return (
        <section id="string-cf">
        <h2>String-Matching Capture Filter Generator</h2>
        <h3>1. Enter the string you want to match</h3>
        <form>
            <p>
                <input
                    type="text"
                    id="searchStr"
                    placeholder="GET /me-a-milkshake-please/ HTTP/1.1"
                    value={searchStr}
                    onChange={(e) => setSearchStr(e.target.value)}
                />
            </p>

            <h3>2. Enter the offset from the start of the TCP data</h3>
            <p>
                <input type="text" id="searchOff" placeholder="0" value={searchOff} onChange={(e) => setSearchOff(e.target.value)} />
            </p>
        </form>
        <button onClick={forceUpdate}>Click to see filter</button>

        <h3>3. Copy the filter below</h3>
        <textarea disabled cols={80} rows={10} class="term" value={filterStr} id="stringFilter"></textarea>

        <h4>What is this?</h4>
        <p>
            It's a web page that lets you create <a
                href="https://gitlab.com/wireshark/wireshark/-/wikis/CaptureFilters"
                >capture filters</a> that match strings in TCP payloads.
        </p>

        <h4>What does it do?</h4>
        <p>
            It takes the string you enter, splits it into 1, 2, or 4 byte
            chunks, converts them to numbers, and creates a capture filter that
            matches those numbers at the offset you provide.
        </p>

        <p>
            It <b>should</b> handle most UTF-8 characters but this hasn't been tested.
        </p>

        <h4>What is it good for?</h4>
        <p>
            You can use it to filter things like top-level HTTP requests ("GET /
            HTTP/1."), HTTP responses ("HTTP/1."), POP3 logins ("USER"), and
            lots of other things.
        </p>

        <h4>What is it NOT good for?</h4>
        <p>
            Matching strings at arbitrary locations. You can't do that with
            capture filters (BPF doesn't support it) You need to use the
            <a
                href="https://www.wireshark.org/docs/man-pages/wireshark-filter.html#search_and_match_operators"
                >"matches" or "contains" display filter operators</a> instead. You'll have to use the "matches" display filter operator for
            case insensitive matching as well.
        </p>

        <h4>
            What's up with all of the fancy bit-twiddling in the TCP header?
        </h4>
        <p>
            It makes sure we skip over any TCP options that might be present.
            See
            <a
                href="https://lists.wireshark.org/archives/wireshark-users/201003/msg00024.html">Sake's explanation</a> for more details.
        </p>

        <h4>Shouldn't this sort of thing be built into Wireshark?</h4>
        <p>Probably.</p>
    </section>
    )
}

export default StringCaptureFilter;