import { FunctionComponent } from "preact";
import { useEffect, useState } from "preact/hooks";

const steps = 25;

const WPAPSK: FunctionComponent = ({ children }) => {
    
    function print_psk(key) {
        document.getElementById("psk").innerHTML = key;
    }

    function status_cb(done) {
        var progress = "";

        for (var i = 0; i < steps; i++) {
            progress += '<span style="width: 5px; background: ';
            if (done - 1.5 > (i * 100.0) / steps) {
                progress += "#11b6f3;";
            } else {
                progress += "#d0fcca;";
            }
            progress += '">&nbsp</span>\n';
        }

        print_psk(progress);
    }

    function gen_psk() {
        var passphrase = document.pskform.passphrase.value;
        var ssid = document.pskform.ssid.value;

        // Sanity checks
        if (!passphrase || !ssid)
            return print_psk("<i>Please fill in both values</i>");

        var pskgen = new PBKDF2(passphrase, ssid, 4096, 256 / 8);
        pskgen.deriveKey(status_cb, print_psk);
    }

    function init_psk() {
        print_psk("<i>Unknown</i>");
    }

    document.addEventListener("DOMContentLoaded", function () {
        init_psk();
    });
    return (
        <section id="wpa-psk">
        <div class="left-side">
            <h3>WPA PSK (Raw Key) Generator</h3>

            <p>
                The Wireshark WPA Pre-shared Key Generator provides an easy way
                to convert a WPA passphrase and SSID to the 256-bit pre-shared
                ("raw") key used for key derivation.
            </p>

            <p>
                <b>Directions</b>:<br />
                Type or paste in your WPA passphrase and SSID below. <strong>Wait a while</strong>. The PSK will be calculated by your browser. Javascript isn't
                known for its blistering crypto speed. <strong>None</strong> of this
                information will be sent over the network. Run a trace with Wireshark
                if you don't believe us.
            </p>
        </div>
        {children}
        <div class="left-side">
            <form name="pskform">
                <table>
                    <tr>
                        <td>Passphrase</td>
                        <td><input type="text" id="passphrase" size={30} /></td>
                    </tr>
                    <tr>
                        <td>SSID</td>
                        <td><input type="text" id="ssid" size={30} /></td>
                    </tr>
                    <tr>
                        <td>PSK</td>
                        <td>
                            <div id="psk"></div>
                        </td>
                    </tr>
                </table>

                <input type="button" value="Generate PSK" onClick={gen_psk} />
            </form>            
            <p>
                This page uses <a href="http://anandam.name/pbkdf2/">pbkdf2.js</a>
                by Parvez Anandam and
                <a href="http://pajhome.org.uk/crypt/md5/">sha1.js</a> by Paul Johnston.
            </p>
            </div>
    </section>
    )
}

export default WPAPSK;