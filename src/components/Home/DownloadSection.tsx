
import styles from "./Homepage.module.scss";
import DownloadAccordion from "../Download/DownloadAccordion";
import SponsorBanners from "../SponsorBanners/SponsorBanners";

const DownloadSection: preact.FunctionComponent = () => {
  return (
    <section id={styles.download}>
      <div id="download">
        <h2>Download Wireshark</h2>
        <div className={styles.downloadWrapper}>
          <DownloadAccordion />
          <SponsorBanners />
        </div>
        <p>
          More downloads and documentation can be found on the{" "}
          <a href="/download.html">downloads page</a>.
        </p>
        <p><a target="_blank" href="https://stratoshark.org">Looking for Stratoshark?</a></p>
      </div>
    </section>
  );
};

export default DownloadSection;
