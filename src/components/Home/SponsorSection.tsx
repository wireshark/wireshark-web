import styles from "./Homepage.module.scss";

const SponsorSection: preact.FunctionComponent = () => {
  return (
    <section id={styles.sponsors}>
      <div className={styles.sponsorWrapper}>
        {/* <Carousel data={sponsorLinks} /> */}
        <span>
          <div>
            <h2>Platinum Member</h2>
            <div>
              <img src="/assets/img/sponsors/endace_big.png" alt="Endace Logo" />
              {/* <img src="/assets/img/sponsors/liveaction.png" alt="LiveAction Logo" /> */}
            </div>
          </div>
          <div>
            <h2>Gold Members</h2>
            <div className={styles.goldMembers}>
              <img src="/assets/img/sponsors/google.png" alt="Google Logo" height={55} />
              <img src="/assets/img/sponsors/profitap-white.png" alt="Profitap Logo" height={55} />
            </div>
          </div>
        </span>
        <p>
          <a id="homeMemberLink" className="aDark" href="/members">
            Meet our members & sponsors
          </a>{" "}
        </p>
      </div>
    </section>
  );
};

export default SponsorSection;
