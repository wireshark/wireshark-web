import styles from "./Homepage.module.scss";
import links from "../../utils/links";

interface DonationSectionProps {
  fromHomepage?: boolean;
}

const DonationSection: preact.FunctionComponent<DonationSectionProps> = ({
  fromHomepage = false,
}) => {
  if (fromHomepage) {
    return (
      <section id={styles.donationSectionHome}>
        <div className={styles.donationWrapper}>
          <div>
            <h1>Support open source packet analysis.</h1>
            <p>
              The non-profit Wireshark Foundation supports the development of
              Wireshark, a free, open-source tool used by millions around the
              world.
            </p>
            <a href={links.donate.url}>
              <button className="primary-button">Make a donation</button>
            </a>
          </div>
          <div></div>
        </div>
      </section>
    );
  } else {
    return (
      <section id={styles.donationSection}>
        <div className={styles.donationWrapper}>
          <div>
            <h2>Support open source packet analysis.</h2>
            <p>
              The non-profit Wireshark Foundation supports the development of
              Wireshark, a free, open-source tool used by millions around the
              world.
            </p>
            <a href={links.donate.url}>
              <button className="primary-button">Make a donation</button>
            </a>
          </div>
          <div></div>
        </div>
      </section>
    );
  }
};

export default DonationSection;
