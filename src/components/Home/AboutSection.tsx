import styles from "./Homepage.module.scss";

const AboutSection: preact.FunctionComponent = () => {
  return (
    <section id={styles.about}>
      <div id="about">
        <h2>What is Wireshark?</h2>
        <p>Wireshark is the world's foremost network protocol analyzer. It lets you see what's happening on your network at a microscopic level. It is the de facto (and often de jure) standard across many industries and educational institutions. It is a free and open-source packet analyzer maintained by a global developer community.</p>
      </div>
    </section>
  );
};

export default AboutSection;
