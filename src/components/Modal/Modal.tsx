import styles from "./Modal.module.scss";
import { ModalData } from "./types";
import useRenderVideoEmbed from "../../hooks/useRenderVideoEmbed";

interface ModalProps {
  title: string;
  subtitle: string;
  link: string;
  modalData: ModalData[];
}

const Modal: preact.FunctionComponent<ModalProps> = ({
  title,
  subtitle,
  link,
  modalData,
}) => {
  const renderModalData = modalData.map((modalContent) => {
    const { link, links, linkEmbed, name } = modalContent;

    const { renderVideoEmbed } = useRenderVideoEmbed(
      linkEmbed,
      "100%",
      "250px"
    );

    return (
      <>
        {!!link ? (
          <a href={link}>
            <h4>{modalContent.name}</h4>
          </a>
        ) : (
          <h4 class={styles.h4Black}>{name}</h4>
        )}
        {linkEmbed && renderVideoEmbed}
        {links && (
          <ul>
            {links.map((linkData) => {
              return (
                <a href={linkData.link}>
                  <li>{linkData.title}</li>
                </a>
              );
            })}
          </ul>
        )}
      </>
    );
  });

  return (
    <>
      <div id={`modal-${title}`} class={styles.modal}>
        <div class={styles.modalContent}>
          <h1>{title}</h1>
          <p>{subtitle}</p>
          <div>{renderModalData}</div>
          <div class={styles.modalFooter}>
            <a href={link}>
              Learn More
            </a>
          </div>
          <a href="#" class={styles.modalClose}>
            &times;
          </a>
        </div>
      </div>
    </>
  );
};

export default Modal;
