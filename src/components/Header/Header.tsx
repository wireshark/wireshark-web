import { FunctionComponent } from "preact";
import styles from "./Header.module.scss";
import links from "../../utils/links";
import Switcher from "../Switcher/Switcher";

interface HeaderProps {
  isLightMode: boolean;
  setIsLightMode: (isLightMode: boolean) => void;
}

const Header: FunctionComponent<HeaderProps> = ({ isLightMode, setIsLightMode }) => {
  return (
    <div className={styles.headerContainer}>
      <div className={styles.topBannerWrapper}>
        {/* Top banner */}
        <a href={links.sharkFest.url}>
          <div className={styles.callToAction}>
          Join us June 14-19 in Richmond, Virginia for SharkFest'25 US, the official Wireshark Developer and User Conference
          </div>
        </a>
        <Switcher isLightMode={isLightMode} setIsLightMode={setIsLightMode} />
      </div>
      <header className={styles.header}>
        <div className={styles.headerWrapper}>
          <a href="/">
            <img
              alt="Wireshark logo"
              src={isLightMode ? "/assets/img/wireshark-logo.png" : "/assets/img/wireshark-logo-light.png"}
              height="40px"
              width="150px"
            />
          </a>
          <input type="checkbox" id={styles.headerMenuToggle} />
          <label for={styles.headerMenuToggle} id={styles.hamburgerLabel}>
            <span className={styles.navicon}></span>
          </label>
          <nav className={styles.mainNav} role="navigation" aria-label="Main">
            <ul>
              <li>
                <a href={links.download.url}>{links.download.name}</a>
              </li>
              <li>
                <a href={links.shop.url}>{links.shop.name}</a>
              </li>
              <li>
                <a href={links.learn.url}>{links.learn.name}</a>
              </li>
              <li>
                <a href={links.about.url}>{links.about.name}</a>
              </li>
              <li>
                <a href={links.blog.url}>{links.blog.name}</a>
              </li>
              <li aria-haspopup="true">
                <a href="#">
                  {links.getHelp.name}
                  <img
                    src={isLightMode ? "/assets/icons/angle-arrow-down.svg" : "/assets/icons/angle-down-white.svg"}
                    alt="Angle arrow down"
                  />
                </a>
                <ul className={styles.mainNavDropDown}>
                  {links.getHelp.subLinks.map((subLink) => (
                    <li>
                      <a href={subLink.url}>{subLink.name}</a>
                    </li>
                  ))}
                </ul>
              </li>
              <li aria-haspopup="true">
                <a href="#">
                  {links.develop.name}
                  <img
                    src={isLightMode ? "/assets/icons/angle-arrow-down.svg" : "/assets/icons/angle-down-white.svg"}
                    alt="Angle arrow down"
                  />
                </a>
                <ul className={styles.mainNavDropDown}>
                  {links.develop.subLinks.map((subLink) => (
                    <li>
                      <a href={subLink.url}>{subLink.name}</a>
                    </li>
                  ))}
                </ul>
              </li>
              <li>
                <a href={links.members.url}>{links.members.name}</a>
              </li>
            </ul>
          </nav>
          <a href={links.donate.url}>
            <button type="button" className={styles.donateButton}>
              {links.donate.name}
            </button>
          </a>
        </div>
      </header>
    </div>
  );
};

export default Header;
