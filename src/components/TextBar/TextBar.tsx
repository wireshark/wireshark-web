import styleVariables from "../../styles/colors.module.scss";
import styles from "./TextBar.module.scss";

interface TextBarProps {
  backgroundColor?: string;
  textColor?: string;
  text: string;
}

const TextBar: preact.FunctionComponent<TextBarProps> = ({
  backgroundColor = styleVariables.colorPrimary,
  textColor = styleVariables.colorWhite,
  text,
}) => {
  return (
    <div className={styles.textBarContainer}>
      <h2 style={{ color: textColor, backgroundColor: backgroundColor }}>
        {text}
      </h2>
    </div>
  );
};

export default TextBar;
