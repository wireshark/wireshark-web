import { useEffect, useState } from "preact/hooks";
import styles from "./Switcher.module.scss"
import { THEME_OPTIONS } from "../../utils/types";

interface SwitcherProps {
  isLightMode: boolean;
  setIsLightMode: (isLightMode: boolean) => void;
}

const Switcher: preact.FunctionComponent<SwitcherProps> = ({ isLightMode, setIsLightMode }) => {

  const handleSwitcherClick = (modeBool: boolean) => {
    if (modeBool) {
      localStorage.setItem("theme", THEME_OPTIONS.DARK);
      setIsLightMode(false);
    } else {
      localStorage.setItem("theme", THEME_OPTIONS.LIGHT);
      setIsLightMode(true);
    }
  };

  return (
    <div className={styles.switchWrapper}>
      {isLightMode && <img onClick={() => handleSwitcherClick(true)} className={styles.sun} src="/assets/icons/phosphor/moon.svg" />}
      {!isLightMode && <img onClick={() => handleSwitcherClick(false)} className={styles.moon} src="/assets/icons/phosphor/sun.svg" />}
    </div>
  )
}

export default Switcher;
