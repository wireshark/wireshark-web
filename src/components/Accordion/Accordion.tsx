import { useState } from "preact/hooks";
import styles from "./Accordion.module.scss";

type AccordionVariants = "bordered" | "simple";

interface AccordionProps {
  title: string;
  content?: { title: string; link: string; icon?: string }[];
  headerContent?: { name: string; subLinks: { name: string; url: string }[] };
  variant: AccordionVariants;
  open?: boolean;
}

const Accordion: preact.FunctionComponent<AccordionProps> = ({
  title,
  content,
  headerContent,
  variant,
  open,
}) => {
  if (typeof open === "undefined") {
    const open = false;
  }

  return (
    <div
      className={
        variant === "bordered"
          ? styles.accordionItemBordered
          : styles.accordionItem
      }
    >
      <details
        className={variant === "bordered" ? styles.accordionTitle : ""}
        open={open}
      >
        <summary>{title || headerContent.name} </summary>
        <div className={styles.accordionContent}>
          <ul>
            {!!content
              ? content.map((item) => (
                  <li>
                    <a href={item.link}>
                      <img src={item.icon} /> {item.title}
                    </a>
                  </li>
                ))
              : headerContent.subLinks.map((headerItem) => (
                  <li>
                    <a href={headerItem.url}>{headerItem.name}</a>
                  </li>
                ))}
          </ul>
        </div>
      </details>
    </div>
  );
};

export default Accordion;
