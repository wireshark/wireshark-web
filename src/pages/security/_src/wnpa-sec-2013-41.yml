file_date: June 7, 2013
fixed_versions: 1.10.1, 1.8.8, 1.6.16
affected_versions: 1.10.0, 1.8.0 to 1.8.7, 1.6.0 to 1.6.15

name: DCP ETSI dissector crash

references: |
    <br/><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=8717">Wireshark bug 8717</a>
    <br/><a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4083">CVE-2013-4083</a>

body: | 
    <p>
    The DCP ETSI dissector could crash.
    <!-- Fixed in trunk: r49802  -->
    <!-- Fixed in trunk-1.10: r49819 -->
    <!-- Fixed in trunk-1.8: r49818 -->
    <!-- Fixed in trunk-1.6: r49822 -->
    </p>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark crash by
    injecting a malformed packet onto the wire or by convincing someone to read a
    malformed packet trace file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark {{ fixed_versions }} or later.
    </p>
