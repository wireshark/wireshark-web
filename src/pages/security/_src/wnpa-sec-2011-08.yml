file_date: May 31, 2011
start_version: 1.4.0
end_version: 1.4.6
fixed_versions: 1.4.7

name: |
    Multiple vulnerabilities in Wireshark

affected_versions: |
    {{ start_version }} up to and including
    {{ end_version }}

related: |
    <a href="wnpa-sec-2011-07.html">wnpa-sec-2011-07</a>
    (Multiple vulnerabilities in Wireshark version 1.2.0 to 1.2.16)

body: |
    <p>
    Wireshark {{ fixed_versions }} fixes the following vulnerabilities:
    </p>
    
    <p>
    <ul>
    
    <li>
        Large/infinite loop in the DICOM dissector.
        <!-- Fixed in trunk: r36958 -->
        <!-- Fixed in trunk-1.4: r37222 -->
        <!-- Fixed in trunk-1.2: r37213 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5876">Bug
        5876</a>)
        </br>
        Versions affected: 1.2.0 to 1.2.16 and 1.4.0 to 1.4.6.</br>
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-1957">CVE-2011-1957</a>
    
    <li>
        Huzaifa Sidhpurwala of the Red Hat Security Response Team discovered
        that a corrupted Diameter dictionary file could crash Wireshark.
        <!-- Fixed in trunk: r37011 -->
        <!-- Fixed in trunk-1.4: r37221 -->
        <!-- Fixed in trunk-1.2: r37213 -->
        </br>
        Versions affected: 1.2.0 to 1.2.16 and 1.4.0 to 1.4.6.</br>
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-1958">CVE-2011-1958</a>
    
    <li>
        Huzaifa Sidhpurwala of the Red Hat Security Response Team discovered
        that a corrupted snoop file could crash Wireshark.
        <!-- Fixed in trunk: r37068 -->
        <!-- Fixed in trunk-1.4: r37252 -->
        <!-- Fixed in trunk-1.2: r37213 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5912">Bug
        5912</a>)
        </br>
        Versions affected: 1.2.0 to 1.2.16 and 1.4.0 to 1.4.6.</br>
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-1959">CVE-2011-1959</a>
    
    <li>
        David Maciejak of Fortinet's FortiGuard Labs discovered that
        malformed compressed capture data could crash Wireshark.
        <!-- Fixed in trunk: r37081 -->
        <!-- Fixed in trunk-1.4: r37257 -->
        <!-- Fixed in trunk-1.2: r37213 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5908">Bug
        5908</a>)
        </br>
        Versions affected: 1.2.0 to 1.2.16 and 1.4.0 to 1.4.6.</br>
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2174">CVE-2011-2174</a>
    
    <li>
        Huzaifa Sidhpurwala of the Red Hat Security Response Team discovered
        that a corrupted Visual Networks file could crash Wireshark.
        <!-- Fixed in trunk: r37128 -->
        <!-- Fixed in trunk-1.4: r37258 -->
        <!-- Fixed in trunk-1.2: r37213 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5934">Bug
        5934</a>)
        </br>
        Versions affected: 1.2.0 to 1.2.16 and 1.4.0 to 1.4.6.</br>
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2175">CVE-2011-2175</a>
    
    </ul>
    </p>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark crash by injecting a series of malformed
    packets onto the wire or by convincing someone to read a malformed packet trace
    file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark {{ fixed_versions }} or later.
    Due to the nature of these bugs we do not recommend trying to work around the
    problem by disabling individual dissectors.</p>
    </p>
    
    
