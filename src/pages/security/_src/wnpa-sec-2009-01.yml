file_date: February 6, 2009
start_version: 0.99.6
end_version: 1.0.5
fixed_versions: 1.0.6

name: |
    Multiple problems in Wireshark

affected_versions: |
    {{ start_version }} up to and including
    {{ end_version }}

body: |
    <p>
    Wireshark {{ fixed_versions }} fixes the following vulnerabilities:
    </p>
    
    <ul>
    
    <li>
      On non-Windows systems, Wireshark could crash if the HOME environment
      variable contained sprintf-style string formatting characters. Discovered by
      babi.
      <!-- Fixed in r26649 -->
      (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3150">Bug
      3150</a>)
      Versions affected: 0.99.8 to 1.0.5
      <!-- <a="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2008-????">CVE-2008-????</a> -->
    
    <li>
      Wireshark could crash while reading a malformed NetScreen snoop file.
      Discovered by babi.
      <!-- Fixed in r27064 -->
      (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3151">Bug
      3151</a>)
      Versions affected: 0.99.7 to 1.0.5
      <!-- <a="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2008-????">CVE-2008-????</a> -->
    
    <li>
      Wireshark could crash while reading a Tektronix K12 text capture file.
      <!-- Fixed in r27064 -->
      (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=1937">Bug
      1937</a>)
      Versions affected: 0.99.6 to 1.0.5
      <!-- <a="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2008-????">CVE-2008-????</a> -->
    
    </ul>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark crash by altering the HOME environment
    variable or by convincing someone to read a malformed packet trace file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark {{ fixed_versions }} or later.
    </p>
    
    <p>
    If are running Wireshark {{ end_version }} or earlier (including Ethereal 0.99.0) and cannot
    upgrade, you can work around each of the problems listed above by doing the
    following:
    </p>
    
    <ul class="item-list">
    
      <li>For each user that will run Wireshark (including root if you're running
      Wireshark as a privileged user), make sure the HOME environment variable
      doesnt' contain any "%" characters.</li>
    
      <li>Don't open any Tektronix K12 text or NetScreen capture files.</li>
    
    </ul>
    
    
    
