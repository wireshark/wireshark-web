file_date: February 1, 2007
fixed_versions: 0.99.5

name: |
    Multiple problems in Wireshark (formerly Ethereal)

affected_versions: |
    0.10.14 up to and including 0.99.4

body: |
    <p>
    Wireshark 0.99.5 fixes the following vulnerabilities:
    </p>
    
    <ul>
    
    <li>
      The TCP dissector could hang or crash while reassembling HTTP packets.
      <!-- Fixed in r19859 -->
      (Bug <href url="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=1200" name="1200">)
      <br>
      Versions affected: 0.99.2 to 0.99.4
      <br>
      <href url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-0459" name="CVE-2007-0459">
    
    <li>
      The HTTP dissector could crash.
      <!-- Fixed in 19899 -->
      <!-- Bug IDs: None -->
      <br>
      Versions affected: 0.99.3 to 0.99.4
      <br>
      <href url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-0458" name="CVE-2007-0458">
    
    <li>
      On some systems, the IEEE 802.11 dissector could crash.
      <!-- Fixed in 20126 -->
      <!-- Bug IDs: None -->
      <br>
      Versions affected: 0.10.14 to 0.99.4
      <br>
      <href url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-0457" name="CVE-2007-0457">
    
    <li>
      On some systems, the LLT dissector could crash.
      <!-- Fixed in 20007 -->
      <!-- Bug IDs: None -->
      <br>
      Versions affected: 0.99.3 to 0.99.4
      <br>
      <href url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-0456" name="CVE-2007-0456">
    
    </ul>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark or Ethereal crash or use
    up available memory by injecting a purposefully malformed packet
    onto the wire or by convincing someone to read a malformed packet
    trace file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark 0.99.5.
    </p>
    
    <p>
    If are running Wireshark 0.99.4 or Ethereal 0.99.0 or earlier and
    cannot upgrade, you can work around each of the problems listed above
    by doing the following:
    </p>
    
    <ul class="item-list">
    
      <li>Disable the HTTP, IEEE 802.11, and LLT dissectors.
    
        <ul class="item-list">
    
          <li>Select <em>Analyze&#8594;Enabled Protocols...</em> from the menu.
    
          <li>Make sure "HTTP", "IEEE 802.11", and "LLT"
          are un-checked.
    
          <li>Click "Save", then click "OK".
    
        </ul>
    
    </ul>
    
    
