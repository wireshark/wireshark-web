/// <reference path="../.astro/types.d.ts" />
// Env variables for intellisense autofill go here

interface ImportMetaEnv {
}

interface ImportMeta {
    readonly env: ImportMetaEnv;
}