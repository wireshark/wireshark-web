export enum THEME_OPTIONS { LIGHT = "theme-light", DARK = "theme-dark" }
export enum MEMBERSHIP_LEVELS {
  PLATINUM = "platinum",
  GOLD = "gold",
  SILVER = "silver"
}

export interface SponsorLink {
  url: string;
  name: string;
  imgPath: string;
  sponsorshipLevel?: string;
}
