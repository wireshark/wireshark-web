<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="generator" content="Asciidoctor 2.0.17">
<title>Wireshark 3.7.0 Release Notes</title>
<link rel="stylesheet" href="./ws.css">
<!--[if IE]><base target="_blank"><![endif]-->
</head>
<body class="article">
<div id="header">
<h1>Wireshark 3.7.0 Release Notes</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>This is an experimental release intended to test new features for Wireshark 4.0.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_what_is_wireshark">What is Wireshark?</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Wireshark is the world’s most popular network protocol analyzer.
It is used for troubleshooting, analysis, development and education.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_whats_new">What’s New</h2>
<div class="sectionbody">
<div class="admonitionblock note">
<table>
<tr>
<td class="icon">
<div class="title">Note</div>
</td>
<td class="content">
We do not ship official packages for 32-bit Windows for this branch.
If you need to use Wireshark on that platform, please use the 3.6 branch.
<a href="https://gitlab.com/wireshark/wireshark/-/issues/17779">Issue 17779</a>
</td>
</tr>
</table>
</div>
<div class="ulist">
<ul>
<li>
<p>The PCRE2 library (<a href="https://www.pcre.org/" class="bare">https://www.pcre.org/</a>) is now a required dependency to build Wireshark.</p>
</li>
<li>
<p>You must now have a compiler with C11 support in order to build Wireshark.</p>
</li>
</ul>
</div>
<div class="paragraph">
<p>Many improvements have been made.
See the “New and Updated Features” section below for more details.</p>
</div>
<div class="sect2">
<h3 id="_new_and_updated_features">New and Updated Features</h3>
<div class="paragraph">
<p>The following features are new (or have been significantly updated) since version 3.6.0:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>The Windows installers now ship with Npcap 1.60.
They previously shipped with Npcap 1.55.</p>
</li>
<li>
<p>The display filter syntax has been updated and enhanced:</p>
<div class="ulist">
<ul>
<li>
<p>A syntax to match a specific layer in the protocol stack has been added.
For example “ip.addr#2 == 1.1.1.1” matches only the inner layer in an IP-over-IP packet.</p>
</li>
<li>
<p>Set elements must be separated using a comma, e.g: {1, 2, "foo"}.
Using only whitespace as a separator was deprecated in 3.6 and is now a syntax error.</p>
</li>
<li>
<p>Support for some additional character escape sequences in double quoted strings has been added.
Along with octal (\&lt;number&gt;) and hex (\x&lt;number&gt;) encoding, the following C escape sequences are now supported with the same meaning: \a, \b, \f, \n, \r, \t, \v.
Previously they were only supported with character constants.</p>
</li>
<li>
<p>Unrecognized escape sequences are now treated as a syntax error.
Previously they were treated as a literal character.
In addition to the sequences indicated above, backslash, single quotation and double quotation mark are also valid sequences: \\, \', \".</p>
</li>
<li>
<p>The display filter engine now uses PCRE2 instead of GRegex (GLib’s bindings to the older and end-of-life PCRE library).
PCRE2 is compatible with PCRE so any user-visible changes should be minimal.
Some exotic patterns may now be invalid and require rewriting.</p>
</li>
<li>
<p>A new strict equality operator "===" or "all_eq" has been added.
The expression "a === b" is true if and only if all a’s are equal to b.
The negation of "===" can now be written as "!==" (any_ne).</p>
</li>
<li>
<p>The aliases "any_eq" for "==" and "all_ne" for "!=" have been added.</p>
</li>
<li>
<p>The operator "~=" is deprecated and will be removed in a future version.
Use "!==", which has the same meaning instead.</p>
</li>
<li>
<p>Dates and times can be given in UTC using ISO 8601 (with 'Z' timezone) or by appending the suffix "UTC" to the legacy formats.
Otherwise local time is used.</p>
</li>
<li>
<p>Integer literal constants may be written in binary (in addition to decimal/octal/hexadecimal) using the prefix "0b" or "0B".</p>
</li>
<li>
<p>A new syntax to disambiguate literals from identifiers has been added.
Every value with a leading dot is a protocol or protocol field.
Every value with a leading colon or in between angle brackets is a literal value.
See the <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChWorkBuildDisplayFilterSection.html#_some_protocol_names_can_be_ambiguous">User’s Guide</a> for details.</p>
</li>
<li>
<p>Floats must be written with a leading and ending digit.
For example the values ".7" and "7." are now invalid as floats.
They must be written "0.7" and "7.0" respectively.</p>
</li>
<li>
<p>The "bitwise and" operator is now a first-class bit operator, not a boolean operator.
In particular this means it is now possible to mask bits, e.g.: frame[0] &amp; 0x0F == 3.</p>
</li>
<li>
<p>Arithmetic is supported for numeric fields with the usual operators “+”, “-”, “*”, “/”, and “%”.
Arithmetic expressions must be grouped using curly brackets (not parenthesis).</p>
</li>
<li>
<p>Logical AND now has higher precedence than logical OR, in line with most programming languages.</p>
</li>
<li>
<p>New display filter functions max(), min() and abs() have been added.</p>
</li>
<li>
<p>Functions can accept expressions as arguments, including other functions.
Previously only protocol fields and slices were syntactically valid function arguments.</p>
</li>
</ul>
</div>
</li>
<li>
<p>The <code>text2pcap</code> command and the “Import from Hex Dump” feature have been updated and enhanced:</p>
<div class="ulist">
<ul>
<li>
<p><code>text2pcap</code> supports writing the output file in all the capture file formats that wiretap library supports, using the same <code>-F</code> option as <code>editcap</code>, <code>mergecap</code>, and <code>tshark</code>.</p>
</li>
<li>
<p><code>text2pcap</code> supports selecting the encapsulation type of the output file format using the wiretap library short names with an <code>-E</code> option, similiar to the <code>-T</code> option of <code>editcap</code>.</p>
</li>
<li>
<p><code>text2pcap</code> has been updated to use the new logging output options and the <code>-d</code> flag has been removed.
The "debug" log level corresponds to the old <code>-d</code> flag, and the "noisy" log level corresponds to using <code>-d</code> multiple times.</p>
</li>
<li>
<p><code>text2pcap</code> and “Import from Hex Dump” support writing fake IP, TCP, UDP, and SCTP headers to files with Raw IP, Raw IPv4, and Raw IPv6 encapsulations, in addition to Ethernet encapsulation available in previous versions.</p>
</li>
<li>
<p><code>text2pcap</code> supports scanning the input file using a custom regular expression, as supported in “Import from Hex Dump” in Wireshark 3.6.x.</p>
</li>
<li>
<p>In general, <code>text2pcap</code> and wireshark&#8217;s “Import from Hex Dump” have feature parity.</p>
</li>
</ul>
</div>
</li>
<li>
<p>The HTTP2 dissector now supports using fake headers to parse the DATAs of streams captured without first HEADERS frames of a long-lived stream (such as a gRPC streaming call which allows sending many request or response messages in one HTTP2 stream).
Users can specify fake headers using an existing stream’s server port, stream id and direction.</p>
</li>
<li>
<p>The IEEE 802.11 dissector supports Mesh Connex (MCX).</p>
</li>
<li>
<p>The “Capture Options” dialog contains the same configuration icon as Welcome Screen.
It is now possible to configure interfaces there.</p>
</li>
<li>
<p>The “Extcap” dialog remembers password items during runtime, which makes it possible to run extcaps multiple times in row.
Passwords are never stored on disk.</p>
</li>
<li>
<p>It is possible to set extcap passwords in <code>tshark</code> and other CLI tools.</p>
</li>
<li>
<p>The extcap configuration dialog now supports and remembers empty strings.
There are new buttons to reset values back to their defaults.</p>
</li>
<li>
<p>Support to display JSON mapping for Protobuf message has been added.</p>
</li>
<li>
<p>macOS debugging symbols are now shipped in separate packages, similar to Windows packages.</p>
</li>
<li>
<p>In the ZigBee ZCL Messaging dissector the zbee_zcl_se.msg.msg_ctrl.depreciated field has been renamed to zbee_zcl_se.msg.msg_ctrl.deprecated</p>
</li>
<li>
<p>The interface list on the welcome page sorts active interfaces first and only displays sparklines for active interfaces.
Additionally, the interfaces can now be hidden and shown via the context menu in the interface list</p>
</li>
<li>
<p>The Event Tracing for Windows (ETW) file reader now supports display IP packets from an event trace logfile or an event trace live session.</p>
</li>
</ul>
</div>
</div>
<div class="sect2">
<h3 id="_removed_features_and_support">Removed Features and Support</h3>
<div class="ulist">
<ul>
<li>
<p>The CMake options starting with DISABLE_something were renamed ENABLE_something for consistency.
For example DISABLE_WERROR=On became ENABLE_WERROR=Off.
The default values are unchanged.</p>
</li>
</ul>
</div>
</div>
<div class="sect2">
<h3 id="_new_protocol_support">New Protocol Support</h3>
<div class="paragraph">
<p>Allied Telesis Loop Detection (AT LDF), AUTOSAR I-PDU Multiplexer (AUTOSAR I-PduM), DTN Bundle Protocol Security (BPSec), DTN Bundle Protocol Version 7 (BPv7), DTN TCP Convergence Layer Protocol (TCPCL), DVB Selection Information Table (DVB SIT), Enhanced Cash Trading Interface 10.0 (XTI), Enhanced Order Book Interface 10.0 (EOBI), Enhanced Trading Interface 10.0 (ETI), FiveCo&#8217;s Legacy Register Access Protocol (5co-legacy), Generic Data Transfer Protocol (GDT), gRPC Web (gRPC-Web), Host IP Configuration Protocol (HICP), Mesh Connex (MCX), Microsoft Cluster Remote Control Protocol (RCP), Realtek, REdis Serialization Protocol v2 (RESP), Secure File Transfer Protocol (sftp), Secure Host IP Configuration Protocol (SHICP), USB Attached SCSI (UASP), and ZBOSS NCP</p>
</div>
</div>
<div class="sect2">
<h3 id="_updated_protocol_support">Updated Protocol Support</h3>
<div class="paragraph">
<p>Too many protocols have been updated to list here.</p>
</div>
</div>
<div class="sect2">
<h3 id="_new_and_updated_capture_file_support">New and Updated Capture File Support</h3>
<div class="paragraph">
<p></p>
</div>
</div>
<div class="sect2">
<h3 id="_major_api_changes">Major API Changes</h3>
<div class="ulist">
<ul>
<li>
<p>proto.h: The field display types "STR_ASCII" and "STR_UNICODE" have been removed.
Use "BASE_NONE" instead.</p>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_getting_wireshark">Getting Wireshark</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Wireshark source code and installation packages are available from
<a href="https://www.wireshark.org/download.html" class="bare">https://www.wireshark.org/download.html</a>.</p>
</div>
<div class="sect2">
<h3 id="_vendor_supplied_packages">Vendor-supplied Packages</h3>
<div class="paragraph">
<p>Most Linux and Unix vendors supply their own Wireshark packages.
You can usually install or upgrade Wireshark using the package management system specific to that platform.
A list of third-party packages can be found on the
<a href="https://www.wireshark.org/download.html">download page</a>
on the Wireshark web site.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_file_locations">File Locations</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Wireshark and TShark look in several different locations for preference files, plugins, SNMP MIBS, and RADIUS dictionaries.
These locations vary from platform to platform.
You can use <span class="menuseq"><b class="menu">Help</b>&#160;<b class="caret">&#8250;</b> <b class="submenu">About Wireshark</b>&#160;<b class="caret">&#8250;</b> <b class="menuitem">Folders</b></span> or <code>tshark -G folders</code> to find the default locations on your system.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_getting_help">Getting Help</h2>
<div class="sectionbody">
<div class="paragraph">
<p>The User’s Guide, manual pages and various other documentation can be found at
<a href="https://www.wireshark.org/docs/" class="bare">https://www.wireshark.org/docs/</a></p>
</div>
<div class="paragraph">
<p>Community support is available on
<a href="https://ask.wireshark.org/">Wireshark’s Q&amp;A site</a>
and on the wireshark-users mailing list.
Subscription information and archives for all of Wireshark’s mailing lists can be found on
<a href="https://www.wireshark.org/lists/">the web site</a>.</p>
</div>
<div class="paragraph">
<p>Bugs and feature requests can be reported on
<a href="https://gitlab.com/wireshark/wireshark/-/issues">the issue tracker</a>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_frequently_asked_questions">Frequently Asked Questions</h2>
<div class="sectionbody">
<div class="paragraph">
<p>A complete FAQ is available on the
<a href="https://www.wireshark.org/faq.html">Wireshark web site</a>.</p>
</div>
</div>
</div>
</div>
<div id="footer">
<div id="footer-text">
Last updated 2022-05-11 17:15:23 UTC
</div>
</div>
</body>
</html>