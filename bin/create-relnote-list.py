#!/usr/bin/env python3

# Imports

import codecs
import glob
import itertools
import json
import locale
import os
import os.path
import profile
import re
import sys

from string import Template

# Global variables

version_re = re.compile(r'wireshark-((\d+\.\d+)\.\d+[^.]*)\.html')

all_versions = []
version_cache = {}

# Functions

# http://www.codinghorror.com/blog/2007/12/sorting-for-humans-natural-sort-order.html
def sort_nicely( l ):
    """ Sort the given list in the way that humans expect.
    """
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    l.sort( key=alphanum_key )
    l.reverse()


# Main program

def main():
    relnote_path = os.path.join(os.path.dirname(__file__), '..', 'src', 'pages', 'docs', 'relnotes')
    # this_path = os.path.join('_bin', os.path.basename(__file__))
    os.chdir(relnote_path)

    with codecs.open(os.path.join('_src', 'release-note-template.astro'), mode='r', encoding='utf-8') as a_tmpl_f:
        a_tmpl_data = a_tmpl_f.read().encode('ascii', 'xmlcharrefreplace').decode('utf-8')

    raw_files = glob.glob('_src/wireshark-*.html')
    sort_nicely(raw_files)

    # Generate our release notes

    relnote_files = []
    for raw_file in raw_files:
        relnote_file = os.path.basename(raw_file)
        ver = version_re.match(relnote_file)
        if ver is None:
            sys.stderr.write('Skipping {}\n'.format(raw_file))
            continue
        with open (raw_file, 'r') as raw_file_f:
            relnote = {
                'version': ver.group(1),
                'raw_file': raw_file,
                'body': raw_file_f.read()
            }

            # with open(relnote_file, 'w') as m_relnote_file_f:
            #     m_relnote_file_f.write(Template(m_tmpl_data).safe_substitute(relnote))

            relnote_basename = os.path.basename(raw_file)
            astro_relnote_file = os.path.splitext(relnote_basename)[0] + '.astro'
            with open(os.path.join(relnote_path, astro_relnote_file), 'w') as a_relnote_file_f:
                a_relnote_file_f.write(Template(a_tmpl_data).safe_substitute(relnote))

            relnote_files.append(relnote_file)

    # Write out our Astro index
    relnotes = []
    branch_init = {'branch': '', 'versions': []}
    branch_data = branch_init.copy()
    for filename in relnote_files:
        res = version_re.match(filename)
        if res is None:
            sys.stderr.write(f'Skipping Astro index {filename}\n')
            continue

        if branch_data['branch'] != res.group(2):
            branch_data = branch_init.copy()
            branch_data['branch'] = res.group(2)
            branch_data['versions'] = []
            relnotes.append(branch_data)

        branch_data['versions'].append(res.group(1))

    astro_relnotes_path = os.path.join(os.path.dirname(__file__), '..', 'src', 'components', 'ReleaseNotes')
    with open(os.path.join(astro_relnotes_path, 'ReleaseNotes.json'), 'w') as astro_relnote_list_f:
        json.dump(relnotes, astro_relnote_list_f, indent=2)


#
# On with the show
#
if __name__ == '__main__':
    profile = 0
    if profile:
        profile.run('main()')
    else:
        sys.exit(main())
