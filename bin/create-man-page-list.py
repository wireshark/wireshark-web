#!/usr/bin/env python3
'''\
Usage:

create-man-page-list.py
'''

# Imports

import glob
import json
import os
import os.path
import re
import sys

# Functions

def exit_err(msg=None):
    if msg: print(msg, file=sys.stderr)
    print(__doc__, file=sys.stderr)
    sys.exit(1)

def main():
    top_dir = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))
    manpage_path = os.path.join(top_dir, 'public', 'docs', 'man-pages')

    manpage_files = glob.glob(os.path.join(manpage_path, '*.html'))
    manpage_files.sort()

    # Write out our Astro list

    manpages = []
    for manpage_file in manpage_files:
        manpage_base = os.path.basename(manpage_file)
        print(('Found ' + manpage_base))
        with open(os.path.join(manpage_path, manpage_file), 'r') as mpf:
            name = os.path.splitext(manpage_base)[0]
            mp_re = re.compile(f'<p>{name} - (.*)</p>')
            for line in mpf:
                line = line.strip()
                m = mp_re.match(line)
                if m:
                    description = m.group(1)
                    if not description.endswith('.'):
                        description += '.'
                    manpages.append({'name': name, 'file_name': manpage_base, 'description': description})
                    break

    with open(os.path.join(top_dir, 'src', 'components', 'ManPages', 'ManPages.json'), 'w') as astro_manpage_list_f:
        json.dump(manpages, astro_manpage_list_f, indent=2)

if __name__ == '__main__':
    main()
