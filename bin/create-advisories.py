#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf8 :

#
# Imports
#

import codecs
import datetime
import glob
import json
import os
import os.path
import re
import sys
import time
import yaml

from string import Template
from types import *

#
# Global variables
#

adv_re = re.compile(r'(wnpa-sec-([12][90]\d\d)-\d\d).yml')

str_keys = [
    'affected_versions',
#    'body',
    'document_id',
    'end_version',
    'file_date',
    'fixed_versions',
    'name',
    'references',
    'related',
    'start_version',
    ]

list_keys = [
    'issue_ids',
    'cve_ids',
]

#
# Functions
#

def exit_err(msg=None):
    if msg: print(msg, file=sys.stderr)
    print(__doc__, file=sys.stderr)
    sys.exit(1)

def prep_str(s):
    s = s.strip()
    s = s.replace('\n', ' ')
    s = s.replace('\r', '')
    s = s.replace("'", r"\'")
    return "'%s'" % (s)

def main():
    top_dir = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))
    security_path = os.path.join(top_dir, 'src', 'pages', 'security')

    yadvisories = glob.glob(os.path.join(security_path, '_src', 'wnpa-sec-20*.yml'))
    yadvisories.sort()
    yadvisories.reverse()

    with codecs.open(os.path.join(security_path, '_src', 'security-advisory-template.astro'), mode='r', encoding='utf-8') as a_tmpl_f:
        a_tmpl_data = a_tmpl_f.read().encode('ascii', 'xmlcharrefreplace').decode('utf-8')

    # Write out our Astro index
    advisories = []
    section_init = {'year': '', 'advisories': []}
    section_data = section_init.copy()
    advisory_init = {
        'related': '',
        'issue_ids': [],
        'cve_ids': [],
        'references': '',
        }

    for _, yadv in enumerate(yadvisories):
        res = adv_re.search(yadv)
        if res is None:
            sys.stderr.write(f'Skipping Astro advisory {yadv}\n')
            continue

        if section_data['year'] != res.group(2):
            section_data = section_init.copy()
            section_data['year'] = res.group(2)
            section_data['advisories'] = []
            advisories.append(section_data)

        with codecs.open(yadv, mode='r', encoding='utf-8') as yadv_f:
            yadv_data = yadv_f.read().encode('ascii', 'xmlcharrefreplace')
        advisory = advisory_init.copy()
        advisory['document_id'] = res.group(1)
        advisory.update(yaml.safe_load(yadv_data))
        advisory['name'] = advisory['name'].rstrip('.')
        advisory['name'] = advisory['name'].rstrip()
        if not advisory['affected_versions']:
            advisory['affected_versions'] = f"{advisory['start_version']} up to and including {advisory['end_version']}"

        # Jinja2 replacements from when we used Mynt
        # XXX We should probably switch to string.Template.
        advisory['body'] = re.sub(r'{{\s*fixed_versions\s+}}', advisory['fixed_versions'], advisory['body'])
        try:
            advisory['affected_versions'] = re.sub(r'{{\s* start_version\s*}}', advisory['start_version'], advisory['affected_versions'])
        except KeyError:
            pass
        try:
            advisory['affected_versions'] = re.sub(r'{{\s* end_version\s*}}', advisory['end_version'], advisory['affected_versions'])
        except KeyError:
            pass

        astro_advisory_file = advisory['document_id'] + '.astro'
        with open(os.path.join(security_path, astro_advisory_file), 'w') as a_advisory_file_f:
            # print(f'Writing {astro_advisory_file}')
            a_advisory_file_f.write(Template(a_tmpl_data).substitute(advisory))

        del advisory['body']
        section_data['advisories'].append(advisory)

    astro_advisories_path = os.path.join(os.path.dirname(__file__), '..', 'src', 'components', 'SecurityAdvisories')
    with open(os.path.join(astro_advisories_path, 'SecurityAdvisories.json'), 'w') as astro_advisory_list_f:
        json.dump(advisories, astro_advisory_list_f, indent=2)


#
# On with the show
#
if __name__ == '__main__':
    profile = 0
    if profile:
        profile.run('main()')
    else:
        sys.exit(main())
