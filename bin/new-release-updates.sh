#!/bin/bash
#

set -e -u -o pipefail

BINDIR=$( dirname "$0" )
ASTRO_SITE_DIR="$BINDIR/../src"

# Make sure we have a working yaml module
if ! python3 -c 'import yaml' 2> /dev/null ; then
    PYTHON_VENV_DIR="$BINDIR/python.venv"
    if ! "$PYTHON_VENV_DIR/bin/python3" -c 'import yaml' 2> /dev/null ; then
        rm -rf "$PYTHON_VENV_DIR"
        python3 -m venv "$PYTHON_VENV_DIR"
        "$PYTHON_VENV_DIR/bin/pip3" install PyYAML
    fi
    export PATH="$PYTHON_VENV_DIR/bin:$PATH"
fi

"$BINDIR/get-version-info.py"
"$BINDIR/create-relnote-list.py"
"$BINDIR/get-authors.py"
"$BINDIR/gen-js-oui.py"
"$BINDIR/gen-appcasts.py"

"$BINDIR/create-advisories.py"
git add "$ASTRO_SITE_DIR/pages/security/wnpa-sec-[0-9][0-9][0-9][0-9]-[0-9][0-9]*.astro" 2> /dev/null

"$BINDIR/create-news.py"
git add "$ASTRO_SITE_DIR/src/pages/news/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]*.astro" 2> /dev/null
git add "$ASTRO_SITE_DIR/src/pages/news/default.astro" 2> /dev/null

curl --fail --location \
    --output "$ASTRO_SITE_DIR/../public/css/ws.css" \
    https://gitlab.com/wireshark/wireshark/-/raw/master/doc/ws.css
git add "$ASTRO_SITE_DIR/../public/css/ws.css"
