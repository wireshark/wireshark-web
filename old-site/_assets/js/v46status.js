// Some code based on http://www.braintrust.co.nz/ipv6wwwtest/

// Requires jQuery.

var site_suf = 'google.com';		// Domain suffix, e.g. example.com
//var site_suf = 'wireshark.org';		// Domain suffix, e.g. example.com
var timeout_ms = 10 * 1000;		// Connectivity timeout, milliseconds
var interval_ms = 250;			// Connectivity check interval, milliseconds
//var img_path = '/images/';		// Path to images
var img_path = '/';			// Path to images
var image ='favicon.ico';

var cur_ms = 0;
var interval_id;
var v4_ok = false;
var v6_ok = false;
var v4_err = false;
var v6_err = false;

function parse_color(color) {
	var copied_from='http://www.adaptavist.com/display/jQuery/Colour+Library';
	// Look for rgb(int,int,int) or rgba(int,int,int,float)
	if ( (m = /^\s*rgb(a)?\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*(?:,\s*([0-9]+(?:\.[0-9]+)?)\s*)?\)\s*$/.exec(color)) && !m[1] === !m[5] ) {
		return [parseInt(m[2],10), parseInt(m[3],10), parseInt(m[4],10), m[5] ? parseFloat(m[5]) : 1];
	}

	// Look for rgb(float%,float%,float%) or rgba(float%,float%,float%,float)
	if ( (m = /^\s*rgb(a)?\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*(?:,\s*([0-9]+(?:\.[0-9]+)?)\s*)?\)\s*$/.exec(color)) && !m[1] === !m[5] ) {
		return [parseFloat(m[2])*255/100, parseFloat(m[3])*255/100, parseFloat(m[4])*255/100, m[5] ? parseFloat(m[5]) : 1];
	}

	// Look for #a0b1c2
	if ( (m = /^\s*#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})\s*$/.exec(color)) ) {
		return [parseInt(m[1],16), parseInt(m[2],16), parseInt(m[3],16), 1];
	}

	// Look for #fff
	if ( (m = /^\s*#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])\s*$/.exec(color)) ) {
		return [parseInt(m[1]+m[1],16), parseInt(m[2]+m[2],16), parseInt(m[3]+m[3],16), 1];
	}

	// Look for hsl(int,float%,float%) or hsla(int,float%,float%,float)
	if ( (m = /^\s*hsl(a)?\(\s*([0-9]{1,3})\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*(?:,\s*([0-9]+(?:\.[0-9]+)?)\s*)?\)\s*$/.exec(color)) && !m[1] === !m[5] ) {
		return [parseInt(m[2],10)/360, parseFloat(m[3])/100, parseFloat(m[4])/100, m[5] ? parseFloat(m[5]) : 1];
	}

	// Force a valid (dark) color.
	return [255,255,255];
}

$.fn.fill_table = function() {
	$(this).html(
		"<table><tbody><tr>" +
		"<td class=\"v4status\">IPv4 ?</td>" +
		"<td class=\"v6status\">IPv6 ?</td>" +
		"</tr></tbody><table>"
		);

}

// We need: start bg, good bg, bad bg.
// We have: start bg.

function set_bg_range(el) {
	var rgb = getComputedStyle(el)["backgroundColor"];
	//var parts = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	var parts = parse_color(rgb);
	// http://stackoverflow.com/questions/596216/formula-to-determine-brightness-of-rgb-color
	// http://en.wikipedia.org/wiki/Luma_(video)
	var luminance = parts[1] * 0.299 + parts[2] * 0.587 + parts[3] * 0.112;

	el.setAttribute('v46_start_r', parts[1]);
	el.setAttribute('v46_start_g', parts[2]);
	el.setAttribute('v46_start_b', parts[3]);
	if (luminance > 127) { // Light background
		el.setAttribute('v46_end_t_r', 0x8a);
		el.setAttribute('v46_end_t_g', 0xe2);
		el.setAttribute('v46_end_t_b', 0x34);
		el.setAttribute('v46_end_f_r', 0xef);
		el.setAttribute('v46_end_f_g', 0x39);
		el.setAttribute('v46_end_f_b', 0x39);
	} else {	// Dark background
		el.setAttribute('v46_end_t_r', 0x4e);
		el.setAttribute('v46_end_t_g', 0x9a);
		el.setAttribute('v46_end_t_b', 0x06);
		el.setAttribute('v46_end_f_r', 0xa4);
		el.setAttribute('v46_end_f_g', 0x00);
		el.setAttribute('v46_end_f_b', 0x00);
	}
}

function set_bg(el, ver, distance) {
	var ok = (ver == 4) ? v4_ok : v6_ok;
	var error = (ver == 4) ? v4_err : v6_err;
	if (ok) {
		el.style.backgroundColor = "rgb(" +
				el.getAttribute('v46_end_t_r') + "," +
				el.getAttribute('v46_end_t_g') + "," +
				el.getAttribute('v46_end_t_b') + ")";
		el.innerHTML = "IPv" + ver + " ✔";
	} else {
		if (distance > 1 || error) {
			distance = 1;
		}
		cur_r = (1 - distance) * el.getAttribute('v46_start_r') + distance * el.getAttribute('v46_end_f_r');
		cur_g = (1 - distance) * el.getAttribute('v46_start_g') + distance * el.getAttribute('v46_end_f_g');
		cur_b = (1 - distance) * el.getAttribute('v46_start_b') + distance * el.getAttribute('v46_end_f_b');
		el.style.backgroundColor = "rgb(" +
				parseInt(cur_r) + "," +
				parseInt(cur_g) + "," +
				parseInt(cur_b) + ")";
		if (distance == 1) {
			el.innerHTML = "IPv" + ver + " ✘";
		}
	}
}

function show_status() {
	var distance = cur_ms / timeout_ms;

	$("#content .v4status").each(function(idx, el) {
		set_bg(el, 4, distance)
	});
	$("#content .v6status").each(function(idx, el) {
		set_bg(el, 6, distance)
	});

	cur_ms += interval_ms;
	//console.log("v46 interval", cur_ms, v4_ok, v4_err, v6_ok, v6_err);
	if (cur_ms >= timeout_ms && (v4_ok || v4_err) && (v6_ok ||v6_err)) {
		clearInterval(interval_id);
		$("#content .v4status").each(function(idx, el) {
			set_bg(el, 4, 1)
		});
		$("#content .v6status").each(function(idx, el) {
			set_bg(el, 6, 1)
		});
	}
	// Fallback in case things go wrong.
	if (cur_ms >= timeout_ms * 2) {
		clearInterval(interval_id);
	}
}


function init_v46() {
	var id = Math.floor(Math.random()*Math.pow(2,31));
//	var v4_url = 'http://ipv4.' + site_suf + img_path + 'ipv4.gif?id=' + id;
//	var v6_url = 'http://ipv6.' + site_suf + img_path + 'ipv6.gif?id=' + id;
//	var v4_url = 'https://ipv4.' + site_suf + img_path + 'google_favicon_128.png?id=' + id;
//	var v6_url = 'https://ipv6.' + site_suf + img_path + 'google_favicon_128.png?id=' + id;
	var v4_url = 'https://ipv4.' + site_suf + img_path + image + '?id=' + id;
	var v6_url = 'https://ipv6.' + site_suf + img_path + image + '?id=' + id;

	//console.log("v46: loading images", v4_url, v6_url);
	$("#v46img").html(
		'<img id="v4img" src="' + v4_url + '" width="1" height="1">' +
		'<img id="v6img" src="' + v6_url + '" width="1" height="1">'
	);

	$("#content .v46status").fill_table();
	$("#content .v4status, #content .v6status").each(function(idx, el) {
		set_bg_range(el)
	});

	$("#v4img").load(function() {
		v4_ok = true;
	});
	$("#v6img").load(function() {
		v6_ok = true;
	});
	$("#v4img").error(function() {
		v4_err = true;
	});
	$("#v6img").error(function() {
		v6_err = true;
	});

	interval_id = setInterval(show_status, interval_ms);
}


// ...and away we go!
if (document.readyState != 'loading'){
	init_v46();
} else {
	document.addEventListener('DOMContentLoaded', init_v46);
}
